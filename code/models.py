# -*- coding: utf-8 -*-
import pyximport; pyximport.install()
from utils import hash_block
from web import RestMixin
import enum
import time
from dataclasses import dataclass, asdict, field
from functools import cached_property
from typing import List
import cython


class Api(enum.Enum):
    CHAIN = "/chain"
    NODES = "/nodes"
    MINE = "/mine"
    TRANSACTIONS = "/transactions"


@dataclass
class DataClassMixin:

    def to_json(self) -> dict:
        return asdict(self)


@dataclass
class Transaction(DataClassMixin):
    content: str


@dataclass
class Block(DataClassMixin):
    index: cython.int = 0
    transactions: List[Transaction] = field(default_factory=list)
    previous_hash: str = "0"
    proof: cython.int = 0
    timestamp: cython.double = time.time()


@dataclass
class BlockChain(DataClassMixin):
    complexity: cython.int = 2
    chain: List[Block] = field(default_factory=list)

    def to_json(self) -> dict:
        return asdict(self).get("chain")

    def __post_init__(self) -> None:
        self.pending_transactions = []
        self.nodes = set()
        self.add_block_(Block())

    @property
    def block(self) -> Block:
        return Block(
                index=len(self.chain),
                transactions=self.pending_transactions,
                previous_hash=self.hash(self.chain[-1]),
                proof=0
            )

    @property
    def len_chain(self) -> cython.int:
        return len(self.chain)

    @property
    def transactions(self) -> List:
        return [ts.to_json() for ts in self.pending_transactions]

    @staticmethod
    def hash(block: Block) -> str:
        return hash_block(block.to_json())

    @cached_property
    def api(self):
        return Api.CHAIN.value

    @cached_property
    def zeros(self) -> str:
        return "0" * self.complexity

    def add_node(self, address: str):
        self.nodes.add(address)

    def add_block_to_chain(self, block: Block) -> bool:
        self.chain.append(block)
        if self.chain_is_valid():
            self.pending_transactions = []
            return True
        self.chain.pop()
        return False

    def add_block_(self, block: Block) -> bool:
        self.reach_consensus_()
        return self.add_block_to_chain(block)

    async def add_block(self, block: Block) -> bool:
        await self.reach_consensus()
        return self.add_block_to_chain(block)

    def add_transaction(self, transaction: Transaction) -> cython.int:
        self.pending_transactions.append(transaction)
        return self.chain[-1].index + 1

    def proof_work(self, block: Block) -> Block:
        while True:
            cur_hash: str = self.hash(block)
            if cur_hash[:self.complexity] == self.zeros:
                break
            block.proof += 1
        return block

    def chain_is_valid(self, external_chain=None) -> bool:
        chain = external_chain or self.chain
        previous_block = chain[0] if isinstance(chain[0], Block) else Block(**chain[0])
        block_index: cython.int = 1
        while block_index < len(chain):
            block: Block = chain[block_index] if isinstance(chain[block_index], Block) else Block(**chain[block_index])
            if block.previous_hash != self.hash(previous_block):
                return False
            if self.hash(block)[:self.complexity] != self.zeros:
                return False
            previous_block: Block = block
            block_index += 1
        return True

    def set_major_chain(self, calls):
        major_chain = self.chain
        for status, resp_json in calls:
            if status == 200:
                chain = resp_json.get('chain')
                if len(chain) > len(major_chain) and self.chain_is_valid(chain):
                    major_chain = chain
        self.chain = major_chain

    def reach_consensus_(self):
        self.set_major_chain(RestMixin.batch_call_(self.nodes, self.api))

    async def reach_consensus(self) -> None:
        self.set_major_chain(
            await RestMixin.batch_call(
                self.nodes,
                self.api,
                None,
                True
                )
            )
