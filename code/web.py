# -*- coding: utf-8 -*-
import asyncio
import multiprocessing
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import Awaitable, Callable
import requests
import aiohttp
from aiohttp.web import Request, Response, json_response


def call_catch(func: Callable[[Request], Awaitable[Response]]) -> Callable[[Request], Awaitable[Response]]:
    async def wrapper(*args, **kwargs) -> Response:
        try:
            return await func(*args, **kwargs)
        except asyncio.CancelledError:
            raise
        except Exception:
            return 500, None
    return wrapper


def call_catch_(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            return 500, None
    return wrapper


def catch(func: Callable[[Request], Awaitable[Response]]) -> Callable[[Request], Awaitable[Response]]:
    async def wrapper(request: Request) -> Response:
        try:
            return await func(request)
        except asyncio.CancelledError:
            raise
        except Exception as e:
            return json_response(RestMixin.render_message(error=str(e)), status=500)
    return wrapper


class RestMixin:

    N_WORKERS = multiprocessing.cpu_count()

    @staticmethod
    def render_message(*args, **kwargs):
        return kwargs

    @classmethod
    async def batch_call(cls, addresses, api, skip_address, return_calls=False, *args, **kwargs):
        calls = await asyncio.gather(*(
            cls.call_node(
                f'{address}{api}',
                *args,
                **kwargs
                ) for address in addresses if address != skip_address
            ))
        if return_calls:
            return calls
        return len(list(filter(lambda r: r[0] == 200, calls))), len(calls)

    @staticmethod
    @call_catch
    async def call_node(address, *args, **kwargs):
        async with aiohttp.ClientSession() as session:
            if kwargs:
                async with session.post(address, **kwargs) as resp:
                    resp_json = await resp.json()
            else:
                async with session.get(address) as resp:
                    resp_json = await resp.json()

            if resp.status == 200:
                return 200, resp_json
            else:
                return 500, None

    @classmethod
    def batch_call_(cls, addresses, api, *args, **kwargs):
        with ThreadPoolExecutor(max_workers=cls.N_WORKERS) as worker:
            for future in as_completed((
                worker.submit(
                    cls.call_node_,
                    f'{address}{api}',
                    *args,
                    **kwargs
                    ) for address in addresses
                )):
                yield future.result()

    @staticmethod
    @call_catch_
    def call_node_(address, *args, **kwargs):
        resp = requests.post(address, **kwargs) if kwargs else requests.get(address)
        if resp.status == 200:
            return 200, resp.json()
        else:
            return 500, None
